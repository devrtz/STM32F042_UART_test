#include "stm32f0xx_hal.h"


extern "C" void SysTick_Handler (void)
{
  HAL_IncTick ();
}

extern "C" void HardFault_Handler (void)
{
  while (1)
    {
    }
}

extern "C" void NMI_Handler (void)
{
  while (1)
    {
    }
}

extern "C" void SVC_Handler (void)
{
}

extern "C" void PendSV_Handler (void)
{
}

extern "C" void USB_IRQHandler (void)
{
}
