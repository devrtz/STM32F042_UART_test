#include "stm32f0xx.h"

#include <string.h>


// Private functions

static void systemclock_config (void);
static void uart_config (void);
static void gpio_config (void);

void error_handler (void);


// Peripherals
UART_HandleTypeDef uart;
bool error;
/**
 * @brief main function
 * @retval exit code (should never really exit)
 */
int main (void) {
  //char buffer[32] = "Hello cruel world!\r\n";
  int i = 0;

  HAL_Init ();

  systemclock_config ();
  //uart_config ();
  //gpio_config ();

    while (1) {

      ++i;
      //HAL_UART_Transmit (&uart, (uint8_t *) buffer, strlen(buffer), 1000);
      //HAL_Delay (1);
    }

    // Return 0 to satisfy compiler
    return 0;
}

/**
 * @brief system clock configuration
 * @retval None
 */
static void systemclock_config (void)
{
  RCC_OscInitTypeDef osc_init;
  RCC_ClkInitTypeDef clk_init;
  RCC_PeriphCLKInitTypeDef periph_init;
  /** Initialize the RCC Oscillators */
  osc_init.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  osc_init.HSEState = RCC_HSE_ON;
  //osc_init.PLL.PLLState = RCC_PLL_NONE;
  osc_init.PLL.PLLState = RCC_PLL_ON;
  osc_init.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  osc_init.PLL.PREDIV = RCC_PREDIV_DIV1;
  osc_init.PLL.PLLMUL = RCC_PLL_MUL6;

  if (HAL_RCC_OscConfig (&osc_init) != HAL_OK)
    {
      // this is bad, mkay
      error_handler ();
      return;
    }
  clk_init.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_SYSCLK;
  clk_init.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  //clk_init.SYSCLKSource = RCC_SYSCLKSOURCE_HSE;
  clk_init.AHBCLKDivider = RCC_SYSCLK_DIV1;
  clk_init.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig (&clk_init, FLASH_LATENCY_1) != HAL_OK)
    {
      // this is bad, mkay
      error_handler ();
      return;
    }

  periph_init.PeriphClockSelection = RCC_PERIPHCLK_USART1;
  periph_init.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;

  if (HAL_RCCEx_PeriphCLKConfig (&periph_init) != HAL_OK)
    {
      // this is bad, mkay
      error_handler ();
      return;
    }
}


/**
 * @brief initialize UART (without DMA for the moment)
 * @retval exit code (should never really exit)
 */
static void uart_config (void)
{
  uart.Instance = USART1;
  uart.Init.BaudRate = 115200;
  uart.Init.WordLength = UART_WORDLENGTH_8B;
  uart.Init.StopBits = UART_STOPBITS_1;
  uart.Init.Parity = UART_PARITY_NONE;
  uart.Init.Mode = UART_MODE_TX_RX;
  uart.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  uart.Init.OverSampling = UART_OVERSAMPLING_16;
  uart.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;

  uart.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;

  if (HAL_UART_Init (&uart) != HAL_OK)
    {
      error_handler ();
    }
}

/**
 * @brief configure GPIO ports
 * retval None
 */
static void gpio_config (void)
{
  ;
}

/**
 * @brief error handler
 * retval None
 */
void error_handler (void)
{
  error = true;
}
