#include "stm32f0xx.h"



extern "C" void HAL_MspInit (void)
{
  __HAL_RCC_SYSCFG_CLK_ENABLE ();
  __HAL_RCC_PWR_CLK_ENABLE ();
}

extern "C" void HAL_MspDeInit (void)
{
  __HAL_RCC_SYSCFG_CLK_DISABLE ();
  __HAL_RCC_PWR_CLK_DISABLE ();
}

extern "C" void HAL_UART_MspInit (UART_HandleTypeDef *huart)
{
  GPIO_InitTypeDef uart1_init; // PA9 + PA10

  if (huart->Instance == USART1) {
    __HAL_RCC_GPIOA_CLK_ENABLE ();
    __HAL_RCC_USART1_CLK_ENABLE ();

    uart1_init.Pin = GPIO_PIN_9 | GPIO_PIN_10;
    uart1_init.Mode = GPIO_MODE_AF_PP;
    uart1_init.Pull = GPIO_PULLUP;
    uart1_init.Speed = GPIO_SPEED_FREQ_HIGH;
    uart1_init.Alternate = GPIO_AF1_USART1;

    HAL_GPIO_Init (GPIOA, &uart1_init);
  }
}


extern "C" void HAL_UART_MspDeInit (UART_HandleTypeDef *huart)
{
  if (huart->Instance == USART1) {
    __HAL_RCC_USART1_CLK_DISABLE ();

    HAL_GPIO_DeInit (GPIOA, GPIO_PIN_9 | GPIO_PIN_10);
  }
}
