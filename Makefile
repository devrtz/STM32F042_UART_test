DEVICE = STM32F042x6
FLASH  = 0x08000000

USE_ST_HAL = true

# Include the main makefile
include ./STM32-base/make/common.mk

CPPFLAGS += -g -DCALL_ARM_SYSTEM_INIT
