# Test UART (STM32-base project)

This is a first test project using the STM32-base project with a STM32F0 series device (STM32F04K6). 
This project uses the [STM32-base project](https://github.com/STM32-base/STM32-base).

UART will run with baudrate of 115200, one stop bit, no parity on Pins PA9 (TX) and PA10 (RX).
